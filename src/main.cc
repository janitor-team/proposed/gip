/*
 *  Author: Samuel Abels <spam debain org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtkmm.h>
#include <libintl.h>
#include "dispatcher.h"
#include "gui_mainwindow.h"

GUIMainwindow *mainwindow;


int main(int argc, char **argv)
{
#ifdef ENABLE_NLS
  bindtextdomain(GETTEXTPACKAGE, PACKAGE_LOCALE_DIR);
  bind_textdomain_codeset(GETTEXTPACKAGE, "UTF-8");
  textdomain(GETTEXTPACKAGE);
#endif
  
  Gtk::Main  gtkmain(argc, argv);
  mainwindow = new GUIMainwindow;
  mainwindow->show();
  Dispatcher dispatcher;
  
  // Run.
  gtkmain.run(*mainwindow);
  
  return 0;
}
