/*
 *  Author: Samuel Abels <spam debain org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifndef HAVE_GUI_IPV4_SUBNET_CALCULATOR_H
#define HAVE_GUI_IPV4_SUBNET_CALCULATOR_H

#include <iostream>
#include <gtkmm.h>
#include <libintl.h>
#include <map>
#include "gui_prefixlist.h"
#include "lib_ipv4.h"
#include <sigc++/sigc++.h>

#define _(String) gettext (String)
#define gettext_noop(String) (String)
#define N_(String) gettext_noop (String)

using namespace std;
using namespace Gtk;

class GUIIPv4SubnetCalculator : public Table {
public:
  GUIIPv4SubnetCalculator();
  ~GUIIPv4SubnetCalculator();
  
  /* Triggered whenever the IP field has been changed. */
  sigc::signal2<void, unsigned long, unsigned long> signal_range_changed;
  
  /* Switch event emissions off. */
  void lock_signals(void);
  
  /* Switch event emissions on (default). */
  void unlock_signals(void);
  
  /* Set the value of the "from" and "to" address range fields. */
  void set_range(unsigned long from, unsigned long to);
  
  GUIPrefixList prefixlist;
  
protected:
  int  map_ip_input(double* value, SpinButton* spin);
  bool map_ip_output(SpinButton* spin);
  void on_range_changed(void);
  
  ScrolledWindow  scrolled;
  Label           label_range;
  Label           label_dash;
  SpinButton      spin_from;
  SpinButton      spin_to;
  bool            lock_events;
};

#endif
