#!/bin/sh
## Checks whether all of the given programs are installed.
## Returns 0 if all of the programs exist.
##
check_progs() {
  PROGS="$@"
  for PROG in $PROGS; do
    which $PROG 1>/dev/null
    if [ "$?" -ne "0" ]; then
      echo "Error: $PROG required but not installed." >&2
      echo "Please install the $PROG program first." >&2
      echo "$PACKAGENAME installation aborted." >&2
      return 1
    else
      echo "Checking $PROG... installed!"
    fi
  done
  return 0
}

check_progs $@ || exit 1
exit 0
