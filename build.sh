#!/bin/bash

# Predefinitions.
EXECUTABLE="gip"
PACKAGENAME="Gip"
VERSION=1.7.0-1
TAGVERSION=""

# Requirements.
REQUIRED_PROGS='getopt basename mkdir find pkg-config g++ intltool-update'
REQUIRED_PROGS="$REQUIRED_PROGS xgettext intltool-update"
REQUIRED_LIBS='gtkmm-2.4 sigc++-2.0'

# Program files.
PROGFILES=`find src/ -name "*.cc" -o -name "*.c"`

# Installation paths. (Plugins will be installed in the LIBDIR)
INST_IN_LIBDIR=`find . -name "*.glade"`
INST_IN_PIXMAPDIR=`find src/ -name "*.png"`
INST_IN_DOCDIR=''
INST_IN_GNOMEDOCDIR=`find doc/ -maxdepth 1 -name "*.xml" -o -path "doc/figures*"`

###############################################################################
#
PATH="$PATH:./installer"
ARGS=$@

## Print the usage informations out.
##
print_usage() {
  (
    echo "Syntax: $0 [options] files"
    echo -e "Options:"
    echo -e "     --dist"
    echo -e "             Create a tarball."
    echo -e "     --force-generate"
    echo -e "             Generate config.h, desktop, and MIME file even if"
    echo -e "             they already exist."
    echo -e "     --prefix PREFIX"
    echo -e "             Installation directory. Default is /usr/local."
    echo -e " -r, --rebuild"
    echo -e "             Delete and rebuild all files."
    echo -e "     --install"
    echo -e "             Install the program (do not not just built)."
  ) >&2
}


## Builds all plugins mentioned in $PLUGINDIRS.
##
build_plugins_all() {
  PLUGINDIRS="$@"
  for PLUGINDIR in $PLUGINDIRS; do          # Compile all plugins.
    PLUGINNAME=`basename $PLUGINDIR`
    echo "####################################################################";
    echo "# Building plugin: $PLUGINNAME";
    echo "####################################################################";
    PLUGINNAMEUC=`echo $PLUGINNAME | tr [:lower:] [:upper:]`
    PLUGINFILES=`find "$PLUGINDIR" -name "*.c" -o -name "*.cc"`
    PLUGINVARNAME="PLUGIN_$PLUGINNAMEUC"
    PLUGINFILES="$PLUGINFILES ${!PLUGINVARNAME}"
    OUTFILE="$PLUGINOUTDIR/$PLUGINNAME.so"
    build_files.sh -s -c "$CFLAGS" -l "$LFLAGS" -o $OUTFILE $PLUGINFILES
    if [ "$?" -ne "0" ]; then
      echo Build failed: $PLUGINNAME plugin. >&2
      return 1
    fi
    echo
  done
  return 0
}


build_tarball() {
  ./clean.sh
  TARBASENAME="$EXECUTABLE-$VERSION"
  mkdir $TARBASENAME
  find . -type d -a ! -path "*$TARBASENAME*" -a ! -path "./.git*" -a ! -path "." \
  | while read DIR; do
    mkdir "$TARBASENAME/$DIR" || return 1
  done
  find . ! -type d -a ! -path "*$TARBASENAME*" -a ! -path "./.git*" \
  | while read FILE; do
    echo Copying $FILE
    cp -r "$FILE" "$TARBASENAME/$FILE" || return 1
  done
  tar czf $TARBASENAME.tar.gz $TARBASENAME
  rm -R $TARBASENAME
  return 0
}


###############################################################################
# Check prerequirements.
###############################################################################
echo "####################################################################";
echo "# Checking prerequired programs.";
echo "####################################################################";
check_programs.sh $REQUIRED_PROGS || exit 1
echo
echo "####################################################################";
echo "# Checking prerequired libraries.";
echo "####################################################################";
check_libs.sh $REQUIRED_LIBS      || exit 1
echo


###############################################################################
# Use getopt to determine the allowed options.
###############################################################################
getopt -l "prefix:,install,force-generate,rebuild,dist" -- "r" $* 1>/dev/null
if [ $? != 0 ]; then
  print_usage
  exit 1
fi


###############################################################################
# This for-loop parses the command line for options and sets a variable
# accordingly.
###############################################################################
# Default options.
REBUILD=0
FORCE_GENERATE=0
INSTALL=0
MAKEDIST=0
INST_PREFIX='/usr/local'

for i in "$@"; do
  case $i in
    "-r" | "--rebuild")
      REBUILD=1
      shift;;
    "--prefix")
      INST_PREFIX="$2"
      shift 2;;
    "--force-generate")
      FORCE_GENERATE=1
      shift;;
    "--install")
      INSTALL=1
      shift;;
    "--dist")
      MAKEDIST=1
      shift;;
    # End of options, just command arguments left.
    "--")
      shift
      break;;
  esac
done

INST_BINDIR="$INST_PREFIX/bin/"
INST_LIBDIR="$INST_PREFIX/lib/$EXECUTABLE"
INST_LOCALEDIR="$INST_PREFIX/share/locale"
INST_PIXMAPDIR="$INST_PREFIX/lib/$EXECUTABLE"
INST_DOCDIR="$INST_PREFIX/doc/$EXECUTABLE"
INST_GNOMEDOCDIR="$INST_PREFIX/share/gnome/help/$EXECUTABLE/C"
INST_MIME_XML="$INST_PREFIX/share/mime/packages/"
INST_MIME_KEY="$INST_PREFIX/share/mime-info/"
INST_MENU_XDG="$INST_PREFIX/share/applications/"
INST_ICON=$INST_PREFIX/share/icons/hicolor
INST_ICON_LOWRES=$INST_ICON/32x32/apps
INST_ICON_HIGHRES=$INST_ICON/48x48/apps

# Preprocessor macros:
MACROS="PACKAGE_LIB_DIR=$INST_LIBDIR/\
:PACKAGE_PIXMAPS_DIR=$INST_PIXMAPDIR/\
:PACKAGE_LOCALE_DIR=$INST_LOCALEDIR/\
:PACKAGE_DOC_DIR=$INST_DOCDIR/\
:VERSION=$VERSION\
:SUBVERSION=$TAGVERSION\
:GETTEXTPACKAGE=$EXECUTABLE\
:ENABLE_NLS=1\
:ENABLE_OGG=1"

DESKTOP_SUB="APP_NAME=$PACKAGENAME\
:INSTALL_BIN=$INST_BINDIR\
:APP_SYSNAME=$EXECUTABLE\
:INSTALL_LIB=$INST_LIBDIR"

PROGRAMNAME="$PACKAGENAME $VERSION $TAGVERSION"


###############################################################################
# Build locales.
###############################################################################
echo "####################################################################";
echo "# Locales.";
echo "####################################################################";
cd po/
echo Generating locales...
./gen.sh || exit 1
cd ..
echo
INST_IN_LOCALEDIR=`find po/ -name "*.mo"`

###############################################################################
# Build a tarball distribution.
###############################################################################
if [ "$MAKEDIST" -eq 1 ]; then
  echo "####################################################################";
  echo "# Building tarball distribution.";
  echo "####################################################################";
  build_tarball || exit 1
  echo Tarball build finished.
  exit 0
fi


###############################################################################
# Cleanup.
###############################################################################
# If specified, clean up first.
if [ "$REBUILD" -eq "1" ]; then
  echo "####################################################################";
  echo "# Cleaning up, as you requested a rebuild."
  echo "####################################################################";
  ./clean.sh || exit 1
  echo
fi


###############################################################################
# Generate config.h and the .desktop file.
###############################################################################
echo "####################################################################";
echo "# Defining Macros.";
echo "####################################################################";
if [ "$FORCE_GENERATE" -eq 1 -o ! -e "$config.h" ]; then
	make_config_h.sh $MACROS | tee config.h || exit 1
	echo "config.h successfully generated."
	echo
fi

if [ "$FORCE_GENERATE" -eq 1 -o ! -e "$EXECUTABLE.desktop" ]; then
	substitute_vars.sh $EXECUTABLE.desktop.in $DESKTOP_SUB | tee $EXECUTABLE.desktop || exit 1
	echo "$EXECUTABLE.desktop successfully generated."
	echo
fi

if [ "$FORCE_GENERATE" -eq 1 -o ! -e "$EXECUTABLE.xml" ]; then
	substitute_vars.sh $EXECUTABLE.xml.in $DESKTOP_SUB | tee $EXECUTABLE.xml || exit 1
	echo "$EXECUTABLE.xml successfully generated."
	echo
fi

###############################################################################
# Build.
###############################################################################
# Compiler/Linker options.
CFLAGS="-DHAVE_CONFIG_H -I. -I.. `pkg-config $REQUIRED_LIBS --cflags`"
LFLAGS=`pkg-config $REQUIRED_LIBS --libs`

if [ "$PLUGINDIRS" != "" ]; then
  build_plugins_all $PLUGINDIRS || exit 1
  INST_IN_LIBDIR="$INST_IN_LIBDIR `find "$PLUGINOUTDIR" -name "*.so"`"
fi


echo "####################################################################";
echo "# Building the program core.";
echo "####################################################################";
build_files.sh -o "$EXECUTABLE" -c "$CFLAGS" -l "$LFLAGS" $PROGFILES || exit 1
echo

echo $PROGRAMNAME compilation finished successfully!
echo


###############################################################################
# Install.
###############################################################################
if [ "$INSTALL" -ne "1" ]; then
  echo For program installation run $0 $ARGS --install
  exit 0
fi

echo "####################################################################";
echo "# Installation in $INST_PREFIX";
echo "####################################################################";
if [ ! -d "$INST_PREFIX" ]; then
  echo "Installation prefix directory $INST_PREFIX does not exist!" >&2
  echo "Use \"--prefix prefix\" to specify an existing directory." >&2
  echo "Installation aborted." >&2
  exit 1
fi

echo Creating directories...
CREATEDIRS="$INST_BINDIR $INST_LIBDIR $INST_PIXMAPDIR $INST_DOCDIR"
CREATEDIRS="$CREATEDIRS $INST_LOCALEDIR $INST_GNOMEDOCDIR"
CREATEDIRS="$CREATEDIRS $INST_MENU_XDG $INST_MIME_XML $INST_GNOMEDOCDIR"
CREATEDIRS="$CREATEDIRS $INST_ICON_LOWRES $INST_ICON_HIGHRES"
make_installdirs.sh $CREATEDIRS                       || exit 1

echo Installing $EXECUTABLE...
cp $EXECUTABLE $INST_BINDIR                           || exit 1

echo Installing libraries in $INST_LIBDIR...
if [ "$INST_IN_LIBDIR" != "" ]; then
  cp $INST_IN_LIBDIR $INST_LIBDIR                     || exit 1
fi

echo Installing pixmaps in $INST_PIXMAPDIR...
if [ "$INST_IN_PIXMAPDIR" != "" ]; then
  cp $INST_IN_PIXMAPDIR $INST_PIXMAPDIR               || exit 1
fi

echo Installing documentation in $INST_DOCDIR...
if [ "$INST_IN_DOCDIR" != "" ]; then
  cp $INST_IN_DOCDIR $INST_DOCDIR                     || exit 1
fi

echo Installing GNOME documentation in $INST_GNOMEDOCDIR...
if [ -d "$INST_GNOMEDOCDIR" -a -w "$INST_GNOMEDOCDIR" ]; then
  cp -r "$INST_IN_GNOMEDOCDIR" "$INST_GNOMEDOCDIR"
else
  echo "WARNING: GNOME documentation directory not found or it is not"  >&2
  echo "         writeable! Skipping GNOME documentation installation." >&2
fi

echo Installing localisation files in $INST_LOCALEDIR...
if [ "$INST_IN_LOCALEDIR" != "" ]; then
  install_locales.sh -p $INST_LOCALEDIR -d $EXECUTABLE $INST_IN_LOCALEDIR \
   || exit 1
fi

echo Installing application icons...
cp src/pixmaps/calc32.png $INST_ICON/32x32/apps/calc.png
cp src/pixmaps/calc48.png $INST_ICON/48x48/apps/calc.png

echo Creating menu entry and registering MIME extension...
cp $EXECUTABLE.desktop $INST_MENU_XDG     || exit 1
cp $EXECUTABLE.xml     $INST_MIME_XML     || exit 1

echo
echo $PROGRAMNAME has been installed successfully!
echo If you find any bugs, please report to the mailinglist at
echo "<gip-devel gna org>".
echo

exit 0
