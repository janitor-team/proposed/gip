#!/bin/sh

# Just to be sure...
if [ ! -e $PWD/clean.sh ]; then
  echo Please enter the Gip package root directory
  echo before starting this script.
  exit 1
fi

echo Deleting all object files starting from $PWD/...
find . -name "*.o" -o -name "*.mo" | while read file; do
  rm $file
done

rm config.h

echo Deleting the Gip binary...
[ -e gip ] && rm gip

echo Deleting desktop and menu file...
[ -e gip.desktop ] && rm gip.desktop
[ -e gip.xml     ] && rm gip.xml
echo Finished.
